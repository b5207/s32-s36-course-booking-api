//[SECTION A.] Dependencies & Modules
	const express = require('express');
	const route = express.Router();
	//Use Pascal case for controller names so that you will remember it's important.
	const CourseController = require('../controllers/coursesController');
	const auth = require('../auth');
	
	//Destructure the actual function that we need to use.
	const {verify, verifyAdmin} = auth;

//[SECTION B.] POST <Create New Course>
	route.post('/create', verify, verifyAdmin, (req, res) => {
		CourseController.addCourse(req.body).then(result => res.send(result))
	});//end of route.post


//[SECTION C.] GET <Retrieve All Courses by Admin>
	route.get('/all', (req, res) => {
		CourseController.getAllCourses().then(result => res.send(result));
	});//end of route.get


//[SECTION D.] GET <Retrieve All ACTIVE Courses>
	route.get('/active', (req, res) => {
		CourseController.getAllActive().then(result => res.send(result));
	});


//[SECTION E.] GET <Retrieve A Specific Course>
	//req.params
	//'/:parameterName' <- this is called a wildcard.
	route.get('/:courseId', (req, res) => {
		console.log(req.params.courseId)
		//we can retrieve the course ID by accessing the request's "params" property that contains all the parameters provided via the URL.
		CourseController.getCourse(req.params.courseId).then(result => res.send(result));
	});//end of route.get


//[SECTION F.] PUT <Updating A Course>
	//what your value is in the wildcard ('/:') should be the same in the req.params.
	route.put('/:courseId', verify, verifyAdmin, (req, res) => {
		CourseController.updateCourse(req.params.courseId, req.body).then(result => res.send(result))
	});//end of route.put


//[SECTION G.] PUT <Archiving A Course>
	route.put('/:courseId/archive', verify, verifyAdmin, (req, res) => {
		CourseController.archiveCourse(req.params.courseId).then(result => res.send(result));
	});//end of route.put


//[SECTION G.] PUT <Activate A Course>
	route.put('/:courseId/activate', verify, verifyAdmin, (req, res) => {
		CourseController.activateCourse(req.params.courseId).then(result => res.send(result));
	});//end of route.put


//[SECTION H.] Expose Route System
	module.exports = route;