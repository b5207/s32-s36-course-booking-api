//[SECTION A.] Dependencies & Modules
	const exp = require('express');
	const controller = require('../controllers/usersController');
	const auth = require('../auth.js');

//[SECTION B.] Routing Component
	const route = exp.Router();


//[SECTION C.] Routes - POST user's registration info 
	route.post('/register', (req, res) => {
		console.log(req.body);
		let userData = req.body;
		//invoke the controller function you wish to execute. Variable name of the controller is in [Section A.]
		controller.register(userData).then(outcome => {
			res.send(outcome);
		});
	}); //end of route.post


//[SECTION D.] Route for User Authentication (login)
route.post('/login', (req, res) => {
	controller.loginUser(req.body).then(result => res.send(result));
});


//[SECTION E.] Routes - GET user's details
route.get('/details', auth.verify, (req, res) => {
	controller.getProfile(req.user.id).then(result => res.send(result));
});

 
//[SECTION F.] Routes - PUT

//[SECTION G.] Routes - DEL


//[SECTION H.] Enroll Our Registered Users
	//Only verified users can enroll in a course.
	route.post('/enroll', auth.verify, controller.enroll);

//[SECTION I.] Get logged user's enrollments
	route.get('/getEnrollments', auth.verify, controller.getEnrollments);


//[SECTION J.] Expose Route System
	module.exports = route;