//[SECTION A.] Dependencies & Modules.
	const jwt = require('jsonwebtoken');
	const dotenv = require('dotenv'); 
	//User defined string data that will be used to create our JSON web tokens.
	//Used in the algorithm for encrypting our data that makes it difficult to decode the information without the defined secret keyword.


//[SECTION B.] Environment Variables Setup
	dotenv.config();
	const mySecret = process.env.SECRET;

//JWT is a way to securely pass information from one part of a server to the frontend or other parts of our application. This will allow us to authorize our users to access or disallow access to certain parts of our app.

//[SECTION C.] Token Creation
	module.exports.createAccessToken = (user) => {
		//check if we can receive the details of the user from our login
		console.log(user);

		//object to contain some details of our user
		const data = {
			id: user._id,
			email: user.email,
			isAdmin: user.isAdmin
		}; //end of const data

		//Generate a JSON web token using the jwt's sign method.
		//you can add {expiresIn: '1h'} in the curly brace to automatically logout a user if inactive for 1 hr.
		return jwt.sign(data,mySecret,{});

	}; //end of module.exports.createAccessToken


//[SECTION D.] Token Verification
	module.exports.verify = (req, res, next) => {
		console.log(req.headers.authorization);
		let token = req.headers.authorization;

		//This if statement will check if the token variable contains undefined or a proper jwt. If it is undefined, we will check token's data type with typeof, then send a message to the client.
		if(typeof token === "undefined"){
			return res.send({auth: "Failed. No token."});
		} else {
			console.log(token);
			//slice(<startingPosition>, <endPosition>)
			token = token.slice(7, token.length);

			//Validate the token using the "verify" method, decrypting the token using secret code.
			jwt.verify(token, mySecret, function(err, decodedToken){
				//err will contain the error from decoding your token. This will contain the reason why the token was rejected.

				//If verification of the token is a sucess, then jwt.verify will return the decoded token.
				if(err){
					return res.send({
						auth: "Failed",
						message: err.message
					}); //end of return statement
				} else {
					console.log(decodedToken) //contains the data from our token.
					req.user = decodedToken; //user property will be added to request object & will contain our decoded token.

					next(); //middleware function	
				}; 

			}); //end of jwt.verify
		}; //end of parent if-else statement
	};// end of module.exports.verify


//[SECTION E.] Verification of An Admin
	module.exports.verifyAdmin = (req, res, next) => {
		//we can get details from req.user because verifyAdmin comes after verify method.
		if(req.user.isAdmin){
			next();
		} else {
			return res.send({
				auth: "Failed",
				message: "Action Forbidden."
			});//end of return
		};//end of if-else
	};//end of module.exports.verifyAdmin