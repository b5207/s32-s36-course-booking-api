//[SECTION A.] Dependencies and Modules
	const express = require ('express');
	const mongoose = require('mongoose');
	const cors = require('cors');
	const dotenv = require('dotenv');
	const userRouter = require('./routes/usersRoute');
	const courseRouter = require('./routes/coursesRoute');

//[SECTION B.] Environment Setup
	dotenv.config();
	let dbCredentials = process.env.CREDENTIALS;
	const port = process.env.PORT;

//[SECTION C.] Server Setup
	const app = express();
	app.use(express.json()); //middleware
	app.use(cors());  //CORS enables all addresses/URL of the client request.
	/* let corsOptions = {
			origin: ['http://localhost:3000', 'http://example.com']
		}
	*/

//[SECTION D.] Database Connection
	mongoose.connect(dbCredentials);
	const connectStatus = mongoose.connection;
	connectStatus.once('open', () => console.log('Database is now connected!'));
	
//[SECTION E.] Backend Routes
	//{4k}users
	app.use('/users', userRouter);
	//{4k}courses
	app.use('/courses', courseRouter);

//[SECTION F.] Server Gateway Response
	app.get('/', (req, res) => {
		res.send(`Welcome to Cube Course Builder!`);
	}); //end of app.get
	app.listen(port, () => {
		console.log(`API is hosted on port ${port}.`)
	}); //end of app.listen


