//[SECTION A.] Dependencies and Modules
	const mongoose = require('mongoose');


//[SECTION B.] Schema/Blueprint
	const courseSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, 'Name is required.']
		},
		description: {
			type: String,
			required: [true, 'Description is required.']
		},
		price: {
			type: Number,
			required: [true, 'Course Price is required.']
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		},
		enrollees: [
			{
				userId: {
					type: String,
					required: [true, 'Student ID is required.']
				}, 
				enrolledOn: {
					type: Date,
					default: new Date()
				}

			} //curly brace closing.
		] //closing array bracket

	});//end of mongoose.Schema


//[SECTION C.] Model
	module.exports = mongoose.model('Course', courseSchema);





