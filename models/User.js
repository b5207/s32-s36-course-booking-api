//[SECTION A.] Dependencies & Modules
	const mongoose = require('mongoose');


//[SECTION B.] Schema/Blueprint
	const userSchema = new mongoose.Schema({
		firstName: {
			type: String,
			required: [true, 'First name is required.']
		},
		lastName: {
			type: String,
			required: [true, 'Last name is required.']
		},
		email: {
			type: String,
			required: [true, 'Email is required.']
		},
		password: {
			type: String,
			required: [true, 'Password is required.']
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		mobileNum: {
			type: String,
			required: [true, 'Mobile number is required.']
		},
		enrollments: [
			{
				courseId: {
					type: String,
					required: [true, 'Course ID is required.']
				},
				courseName: {
					type: String,
					required: [true, 'Course Name is required.']
				},
				courseDesc: {
					type: String,
					required: [true, 'Course Description is required.']
				},
				coursePrice: {
					type: Number,
					required: [true, 'Course Price is required.']
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				}, 
				status: {
					type: String,
					default: 'Enrolled'
				}

			} //closing curly bracket
		] //array closing bracket
	}); //end of mongoose.Schema


//[SECTION C.] Model
	module.exports = mongoose.model('User', userSchema);