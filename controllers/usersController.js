//[SECTION A.] Dependencies & Modules
	const User = require('../models/User'); 
	const Course = require('../models/Course'); //added this new module
	const bcrypt = require('bcrypt'); 
	const dotenv = require('dotenv'); 
	const auth = require('../auth.js');

//[SECTION B.] Environment Variables Setup
	dotenv.config();
	const asin = parseInt(process.env.SALT);


//[SECTION I.] FUNCTIONALITIES <CREATE>
	//1. Register New Account
	module.exports.register = (userData) => {
		let fName = userData.firstName;
		let lName = userData.lastName;
		let email = userData.email;
		let pass = userData.password;
		let mobile = userData.mobileNum;

		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(pass, asin),
			mobileNum: mobile
		});//end of let newUser

		return newUser.save().then((fulfilled, rejected) => {
			if (fulfilled) {
				return fulfilled;
			} else {
				return {message: 'Failed to register new account.'};
			};//end of if-else

		}); //end of return newUser

	};//end of module.exports.register


//[SECTION II.] FUNCTIONALITIES <RETRIEVE>
	/*STEPS:
	1. Find the document in the database using the user's ID.
	2. Reassign the password of the returned document to an empty string.
	3. Return the result back to the client.*/
	module.exports.getProfile = (data) => {
		return User.findById(data).then(result => {
			//change the value of the user's password to an empty string.
			result.password = '';
			return result;
		}); //end of return statement
	}; //end of module.export.getProfile


//[SECTION III.] FUNCTIONALITIES <UPDATE>


//[SECTION IV.] FUNCTIONALITIES <DELETE>


//[SECTION V.] User Authentication
	/*
	STEPS:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the db.
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not.
	*/
	module.exports.loginUser = (data) => {
		//findOne method returns the 1st record in the collection that matches the search criteria.
		return User.findOne({email: data.email}).then(result => {
			//if User *does not exist*
			if(result === null) {
				return false;
			} else {
				//else User *exists*
				//'compareSync' method from the bcrypt to be used in comparing the non-encrypted password from the login and the database password.
				const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)

				//if the password matched, return token
				if(isPasswordCorrect){
					return { accessToken: auth.createAccessToken(result.toObject())}
				} else {
					//password does not match
					return false;
				}
			}; //end of parent if-else

		}); //end of return statement
	}; //end of module.exports.loginUser


//[SECTION VI.] ENROLL Registered User
	/*ENROLLMENT STEPS:
		1. Look for the user by its ID.
			- Push the details of the course we're trying to enroll in. We'll push the details to a new enrollment subdocument in our user.
		2. Look for the course by its ID.
			- Push the details of the enrollee who's trying to enroll. We'll push to a new enrollees subdocument in our course.
		3. When both saving documents are successful, we will send a message to the client. TRUE if successful, FALSE if not.
	*/

	module.exports.enroll = async (req, res) => {
		//console.log("Test enroll route");
		console.log(req.user.id); //the user's id from the decoded token after verify()
		console.log(req.body); //the course ID from our request body.

		if(req.user.isAdmin) {
			return res.send({message: "You are an admin. Action Forbidden."})
		};//end of if statement

		//GET the user's ID to save the courseID inside the enrollments field.
		let isUserUpdated = await User.findById(req.user.id).then(user => {
			//Add the courseId in an object and push that object into user's enrollments array.

			let newEnrollment = {
				courseId: req.body.courseId,
				courseName: req.body.courseName,
				courseDesc: req.body.courseDesc,
				coursePrice: req.body.coursePrice
			};
			//SYNTAX: user."property name in model schema".push();
			user.enrollments.push(newEnrollment);
			console.log(user.enrollments);
			//Save the changes made in our user document.
			return user.save().then(user => true).catch(err => err.message)

			//If isUserUpdated does not return the boolean "true", we will stop our process and return a message to our client.
			if(isUserUpdated !== true) {
				return res.send({message: isUserUpdated})
			};

		});//end of let isUserUpdated

		//FIND the Course ID that we need to push to our enrollee.
		let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
			//create an object which will be pushed to enrollee array.
			let enrollee = {
				userId: req.user.id
			}
			course.enrollees.push(enrollee);

			//save the course document
			return course.save().then(course => true).catch(err => err.message)

			if(isCourseUpdated !== true) {
				return res.send({message: isCourseUpdated});
			}
		});//end of let isCourseUpdated

		//send message to the client that we have successfully enrolled our user if both isUserUpdated and isCourseUpdated contain the boolean true.

			if(isUserUpdated && isCourseUpdated) {
				return res.send({ message: "Enrolled Successfully" })
			}
	};//end of module.exports.enroll



//[SECTION VII.] <GET> User's Enrollments
	module.exports.getEnrollments = (req, res) => {
			User.findById(req.user.id)
			.then(result => res.send(result.enrollments))
			.catch(err => res.send(err))
	}
