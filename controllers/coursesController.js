//[SECTION A.] Dependencies & Modules
	const Course = require('../models/Course');

//[SECTION B.] POST <Create a New Course>
	/*
	Steps:
		1. Create a new Course object using the mongoose model and the info from the request body.
		2. Save the new Course to the database.
	*/
	module.exports.addCourse = (reqBody) => {
		//Create a variable: "newCourse" and instantiate the name, desc, and price.
		let newCourse = new Course ({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		}); 
		//Save the created object to our database.
		return newCourse.save().then((course, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		}).catch(error => error)
	}; //end of module.exports.addCourse


//[SECTION C.] GET <Retrieve All Courses by Admin>
	module.exports.getAllCourses = () => {
		return Course.find({}).then(result => {
			return result;
		})
	}; //end of module.exports.getAllCourses


//[SECTION D.] GET <Retrieve All ACTIVE Courses>
	//1. Retrieve all the courses with the property isActive: true
	module.exports.getAllActive = () => {
		return Course.find({isActive: true}).then(result => {
			return result;
		}).catch(error => error)
	}; //end of module.exports.getAllActive


//[SECTION E.] GET <Retrieve a Specific Course>
	//1. Retrieve the cours that matches the course ID provided from the URL.
	module.exports.getCourse = (reqParams) => {
		return Course.findById(reqParams).then(result => {
			return result;
		}).catch(error => error)
	}; //end of module.exports.getCourse


//[SECTION F.] PUT <Update A Course>
	/*STEPS:
		1. Create a variable "updatedCourse" that will contain info from the req.body
		2. Find and update the course using the courseId retrieved from the req.params and the variable "updatedCourse" containing info from req.body.
	*/
	module.exports.updateCourse = (courseId, data) => {
		//Specify the fields/properties of the document to be updated.
		let updatedCourse = {
			name: data.name,
			description: data.description,
			price: data.price
		}
		//findByIdAndUpdate(documentId, updatesToBeApplied)
		return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
			if(error){
				return false;
			} else{
				return true;
			}
		}).catch(error => error)
	};//end of module.exports.updateCourse

 
//[SECTION G.] PUT <Archiving A Course>
	//1. Update the status of "isActive" into false which will no longer be displayed in the client whenever all active courses are retrieved.
	module.exports.archiveCourse = (courseId) => {
		let updateActiveField = {
			isActive: false
		};
		return Course.findByIdAndUpdate(courseId, updateActiveField).then((course, error) => {
			if(error){
				return false;
			} else {
				return updateActiveField;
			}
		}).catch(error => error)
	}; //end of module.exports.archiveCourse


//[SECTION G.] PUT <Activating A Course>
	module.exports.activateCourse = (courseId) => {
		let updateActiveField = {
			isActive: true
		};
		return Course.findByIdAndUpdate(courseId, updateActiveField).then((course, error) => {
			if(error){
				return false;
			} else {
				return updateActiveField;
			}
		}).catch(error => error)
	}; //end of module.exports.activateCourse
